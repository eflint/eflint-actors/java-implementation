### Installation

Can be installed for use with `sbt` using `sbt publishLocal` or can be run in Eclipse.

Both require the `eflint-server` executable (installation readme [here](https://gitlab.com/eflint/haskell-implementation)) to be available in PATH.