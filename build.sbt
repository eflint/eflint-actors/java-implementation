scalaVersion := "2.13.3"
ThisBuild / organization := "eflint"
ThisBuild / version := "0.1.11-SNAPSHOT"
name := "java-server"

Compile / javaSource := baseDirectory.value / "src"
unmanagedJars in Compile += file("assets/json-simple-1.1.1.jar")
