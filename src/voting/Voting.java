package voting;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import util.EFLINT;
import util.EFLINTInstance;
import util.InvalidEFlintInput;
import util.InvalidEFlintTransitionAttempt;
import util.Value;

class Voting implements EFLINTInstance {
  Set<String> registered_voters = new HashSet<String>();
  Map<String,Integer> vote_count = new HashMap<String, Integer>();
  private final EFLINT monitor;

  public Voting() { 
    this.monitor = new EFLINT("src/voting/voting.eflint");
  }
 
  public void register_voter(String name) {
    registered_voters.add(name);
    // qualification
    try {
      monitor.action("enable-vote", "Admin", name).print_violations();
    }catch(InvalidEFlintTransitionAttempt e) {}
  }

  public void register_candidate(String name) {
    vote_count.put(name,0);
  }

  public void vote(String voter, String candidate) throws InvalidEFlintInput {
    try {
      monitor.action("cast-vote", true, voter, "Admin", new Value("candidate", candidate)).print_violations();
      vote_count.put(candidate, vote_count.getOrDefault(candidate,0) + 1);
    }catch(InvalidEFlintTransitionAttempt e) {
      System.out.println("no voting power for " + voter);
    }
  }

  // returns null if there is no winner
  public String find_winner() {
    String winner = null;
    int winning_votes = 0;
    for (Entry<String,Integer> entry : vote_count.entrySet()) {
      if(entry.getValue() > winning_votes) {
        winner = entry.getKey();
        winning_votes = entry.getValue();
      }
    }
    // qualification
    if (winner != null) {
      try {
        monitor.action("declare-winner", "Admin", winner).print_violations();
        return winner;
      }catch(InvalidEFlintTransitionAttempt e) {
        System.out.println(winner + " cannot be declared winner");
      }
    }
    return null;
  }
  
  public EFLINT eFLINT() {
    return this.monitor;
  }
}