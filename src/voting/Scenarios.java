package voting;

import java.util.*;

import util.*;

class Scenarios { // fails because an unregistered voter has voted
  
  public static void main(String[] args) {
    Voting voting = new Voting();
    // init
    voting.register_candidate("Chloe");
    voting.register_candidate("David");
    voting.register_candidate("Hank");
    voting.register_voter("Alice");
    voting.register_voter("Bob");
    voting.register_voter("John");
    voting.register_voter("Frank");
    int state_id = voting.eFLINT().get_state_id();
    
    System.out.println("== scenario 0");
    voting.vote("Alice", "Chloe");
    voting.vote("Bob", "David");
    voting.vote("John", "David");
    String winner = voting.find_winner();
    System.out.println("winner is: " + winner); 
    if(!voting.eFLINT().test_present(new Value("winner", winner))) {
      System.out.println(winner + " did not really win!");
    }
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();
    
    System.out.println("== scenario 1");
    voting.vote("Alice", "Chloe");
    voting.vote("Bob", "Chloe");
    voting.vote("Unknown", "Chloe"); // no voting power for Unknown
    winner = voting.find_winner();
    System.out.println("winner is: " + winner );
    if(!voting.eFLINT().test_present(new Value("winner", winner))) {
      System.out.println(winner + " did not really win!");
    }
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();
    
    System.out.println("== scenario 2");
    voting.vote("Bob", "Hank");
    voting.vote("John", "Hank");
    voting.vote("Alice", "Chloe");
    voting.vote("Frank", "David");
    voting.vote("Alice", "David"); // no voting power for Alice (anymore)
    winner = voting.find_winner(); // but forced so hank and david should have equal amount of votes, and no winner can thus be declared
    if (winner != null)
      System.out.println("winner is: " + winner);
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();
    
    System.out.println("== scenario 3");
    voting.vote("Alice", "Chloe");
    voting.vote("Bob", "David");
    winner = voting.find_winner(); // no power to declare winner (equal amount of votes)
    if (winner != null)
      System.out.println("winner is: " + winner);
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();
    
    System.out.println("== scenario 4");
    voting.vote("Alice", "Chloe");
    voting.vote("Bob", "David");
    voting.vote("John", "David");
    voting.vote("Frank", "Chloe");
    winner = voting.find_winner();  // no power to declare winner (equal amount of votes)
    if (winner != null)
      System.out.println("winner is: " + winner);
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();

    System.out.println("== scenario 5");
    voting.vote("Alice", "Chloe");
    voting.vote("Bob", "David");
    weeks_later(voting.eFLINT(),2).print_violations(); // frank and john violated the voting duty
    voting.vote("John", "Chloe"); // john no longer violated the duty (but frank did)
    winner = voting.find_winner();
    if (winner != null)
      System.out.println("winner is: " + winner);
    voting.eFLINT().revert(state_id);
    voting.vote_count = new HashMap<String, Integer>();   

    System.out.println("== scenario 6");
    find_winner(voting); // no winner
    voting.vote("Alice", "Chloe");
    find_winner(voting); // Chloe
    voting.vote("Bob", "David");
    find_winner(voting); // no winner
    voting.vote("John", "David");
    find_winner(voting); // David
    voting.vote("Frank", "Chloe");
    find_winner(voting); // no winner
    voting.eFLINT().close();
  }
  
  public static ServerState weeks_later(EFLINT m, int weeks) {
    for (int i = 0; i < weeks; i++) {
      if (m.test_present(new Value("weeknr", i))) {
        m.terminate(new Value("weeknr", i));
        break;
      }
    }
    return m.create(new Value("weeknr", weeks));
  }
  
  // query the server repeatedly to see if there is a candidate with the most votes
  // effectively answering the query `?most-votes-on(X).`
  public static void find_winner(Voting v) {
    for (String candidate : v.vote_count.keySet()) {
      if (v.eFLINT().test_present(new Value("most-votes-on", candidate))) {
        System.out.println(candidate); return;
      }
    }
    System.out.println("no possible winner yet");
  }
}
