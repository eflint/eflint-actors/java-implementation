package util;
import java.io.*;
import java.net.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.*;
import org.json.simple.parser.*;

public class EFLINT implements AutoCloseable {
    
  private static final int PORT_MIN_NUM = 20000;
  private static final int PORT_MAX_NUM = 30000;
  private static final long CONNECTION_WAIT = 50;
  private static final long CONNECTION_WAIT_MAX = 10000;
  private final JSONParser parser = new JSONParser();
  private final int port;
  private long pid;
  private boolean closed = false;
  private boolean debug_mode = false;

  public EFLINT() {
    this(null);
  } 

  public EFLINT(String filepath) {this(filepath, false);}

  public EFLINT(String filepath, boolean debug){
    this.debug_mode = debug;
    this.port = getRandomPort();
    File eflint_file = new File(filepath);
    if (!eflint_file.exists() || !eflint_file.canRead() || !eflint_file.getName().endsWith(".eflint")) {
      System.out.println("not a known eflint file: " + filepath);
    }
    String[] command = {"eflint-server", filepath.isEmpty() ? "" : filepath, ""+this.port};
    Process eflint_server = null;
    try {
      eflint_server = Runtime.getRuntime().exec(command);
      this.pid = eflint_server.pid();
    }catch (IOException e) {
      System.out.println("cannot execute command: " + command);
      System.out.println(e.getMessage());
      System.exit(1);
    }
    ServerState status = get_status();
    if (status == null) {
      System.out.println("server with PID: " + this.pid + " did not respond with \"success\"");
      this.close();
    }
    if (debug_mode) System.out.println("eFLINT instance for \"" + filepath + "\" ready at port " + this.port + " with PID " + eflint_server.pid());
  }

  private JSONObject communicate(JSONObject input) throws InvalidEFlintInput {
    return communicate(input, 0);
  }

  private JSONObject communicate(JSONObject input, long waited) 
    throws InvalidEFlintInput
   {
    if (this.isClosed()) {
      return null;
    }
    try (Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), port)
        ;PrintWriter writer = new PrintWriter(socket.getOutputStream())
        ;BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
      String response_str = null;
      try {
        writer.println(input.toString());
        writer.flush();
        response_str = reader.readLine();
        if (response_str != null)  {
          JSONObject response = (JSONObject) parser.parse(response_str);
          if(response.get("response") != null && response.get("response").equals("invalid input")) {
            if (response.get("error") == null)
              throw new InvalidEFlintInput();
            else
              throw new InvalidEFlintInput(response.get("error").toString());
          }
          else if (response.get("response") != null && response.get("response").equals("invalid command")) {
              System.out.println("unexpected error: " + response.get("message"));
              this.close();
              System.exit(1);
              return null;
          } 
          else if (response.get("response") != null && response.get("response").equals("input required")) {
            List<Value> missing = (List<Value>) ((JSONArray) response.get("values")).stream().map(o -> Value.fromJSON((JSONObject) o)).collect(Collectors.toList());
            throw new MissingInputRequired(missing);
          }
          return response;
        } else {System.out.println("communication error for PID " + this.pid + " on port " + port);}
      }catch(ParseException e) {
        System.out.println("incorrect response format for PID " + this.pid + " on port " + port);
        System.out.println(response_str);
      }
    }catch(ConnectException e) {
      if (waited < CONNECTION_WAIT_MAX) {
        try {
          Thread.sleep(CONNECTION_WAIT);
        } catch (InterruptedException e1) {
          e1.printStackTrace();
        }
        return communicate(input, waited + CONNECTION_WAIT);
      }
      else {
        System.out.println("stopped waiting to connect to PID " + this.pid + " on port " + port);
        //this.close();
        System.exit(1);
      }
    }catch(IOException e) {
      System.out.println("cannot communicate with PID " + this.pid + " on port " + port);
      System.out.println(e);
      e.printStackTrace();
      this.close();
      System.exit(1);
    }
    return null;
  }

  
  public void reset() throws InvalidEFlintInput {
    revert(-1);
  }
  public void revert() throws InvalidEFlintInput {
    revert(-1);
  }
  public void revert(int sid) throws InvalidEFlintInput
  {
    JSONObject obj = new JSONObject();
    obj.put("command", "revert");
    obj.put("value", sid);
    communicate(obj);
  }

  public ServerState action(Value val) 
      throws InvalidEFlintInput, InvalidEFlintTransitionAttempt
  {
      return trigger(val, false, false);
  }
  
  public ServerState action(Value val, boolean force) 
    throws InvalidEFlintInput, InvalidEFlintTransitionAttempt
  {
    return trigger(val, false, force);
  }
  
  public ServerState event(Value val)
    throws InvalidEFlintInput, InvalidEFlintTransitionAttempt
  {
    return trigger(val, true);
  }
  
  public ServerState trigger(Value val, boolean event) 
    throws InvalidEFlintInput, InvalidEFlintTransitionAttempt {
    return trigger(val, event, false);
  }
    
  public ServerState trigger(Value val, boolean event, boolean force) 
    throws InvalidEFlintInput, InvalidEFlintTransitionAttempt
  {
    JSONObject obj = new JSONObject();
    obj.put("command", event ? "event": "action");
    obj.put("value", val.toJSON());
    if (!event && force) {
      obj.put("force", "true");      
    }
    ServerState ss = handle_response(communicate(obj));
    return ss;
  }

  public ServerState action(String act_type, String actor, String recipient, Value... related) 
      throws InvalidEFlintInput, InvalidEFlintTransitionAttempt {
    return action(act_type, false, actor, recipient, related);
  }
  
  public ServerState action(String act_type, boolean force, String actor, String recipient, Value... related) 
    throws InvalidEFlintInput, InvalidEFlintTransitionAttempt
  {
    JSONObject obj = new JSONObject();
    obj.put("command", "action");
    obj.put("act-type", act_type);
    obj.put("actor", actor);
    obj.put("recipient", recipient);
    obj.put("force", force);
    JSONArray json_rels = new JSONArray();
    for (Value rel : related)
      json_rels.add(rel.toJSON());
    obj.put("objects", json_rels);
    ServerState ss = handle_response(communicate(obj));
    return ss;
  }

  private ServerState handle_response(JSONObject response) {
    if(response.get("response").equals("success")) {
      return new ServerState(response);
    }
    return null;
  }

  public ServerState create(Value value) 
    throws InvalidEFlintInput
  {
    return this.single_fact_event("create", value); 
  }

  public ServerState terminate(Value value) 
    throws InvalidEFlintInput
  {
    return this.single_fact_event("terminate", value);
  } 

  private ServerState single_fact_event(String s, Value value) 
    throws InvalidEFlintInput
  {
    JSONObject obj = new JSONObject();
    obj.put("command", s);
    obj.put("value", value.toJSON());
    JSONObject response = communicate(obj);
    if (response.get("response") == null || response.get("response").equals("success"))
      return new ServerState(response);
    return null;
  }

  public boolean test_absent(Value val) {
    return test_absent(val, null);
  }
  public boolean test_absent(Value value, List<Assignment> input) {
      return query_phrase("?Not(" + value.toString() + ")", input);
  }
   
  public boolean test_present(Value value) {
    return test_present(value, null);
  } 
  public boolean test_present(Value value, List<Assignment> input) {
      return query_phrase("?" + value.toString(), input);
  }

  public boolean enabled(Value value) {
    return enabled(value, null);
  }
  public boolean enabled(Value value, List<Assignment> input) {
    return query_phrase("?Enabled(" + value.toString() + ")", input); 
  }
   
  public boolean query_phrase(String string) {
    return query_phrase(string, null);
  } 
  public boolean query_phrase(String string, List<Assignment> input)  {
    try {
      ServerState res = phrase(string, input);
      return res.lastQuerySucceeded();
    } catch (InvalidEFlintInput exc) {
      System.out.println("invalid phrase input: " + string);
    }
    return false;
  }

  public ServerState phrase(String string) 
      throws InvalidEFlintInput {
    return phrase(string, null);
  }
  
  public ServerState phrase(String string, List<Assignment> input) 
      throws InvalidEFlintInput {
    JSONObject obj = new JSONObject();
    obj.put("command", "phrase");
    obj.put("text", string);
    if (input != null && !input.isEmpty())
      obj.put("input", input.parallelStream().map(a -> a.toJSON()).collect(Collectors.toList()));  
    return handle_response(communicate(obj));
  }
  
  public ServerState try_phrase(String string, List<Assignment> input) {
    ServerState ss = this.phrase(string, input);
    this.revert(ss.getOldSID());
    return ss;
  }

  public List<Value> instance_query(String string) {
    return instance_query(string, null);
  }
  
  public List<Value> instance_query(String string, List<Assignment> input) 
    throws InvalidEFlintInput {
    ServerState ss = phrase("?-" + string, input);
    return ss.get_query_instances();
  }
  
  public void close() {
    if (this.closed) return; 
    JSONObject obj = new JSONObject();
    obj.put("command", "kill");
    JSONObject response = communicate(obj);
    if (response.get("response") == null) {
      System.out.println("could not kill server with PID " + this.pid);
    }
    else if (response.get("response").equals("bye world..")) {
      this.closed = true;
      if (debug_mode) System.out.println("successfully killed server with PID " + this.pid);

    } else {
      System.out.println("unknown response: " + response.get("response"));
    }
  }
  
  public boolean isClosed() {
    return this.closed;
  }

  public void instances_of(String type, Object... instances) {
    JSONObject obj = new JSONObject();
    obj.put("command", "instances-of");
    obj.put("fact-type", type);
    JSONArray arr = new JSONArray();
    for (Object instance : instances) {
      arr.add(instance);
    }
    obj.put("instances", arr);
    JSONObject response = communicate(obj);
  }
  
  public List<Value> get_facts() {
    List<Value> res = new ArrayList<Value>();
    JSONObject obj = new JSONObject();    
    obj.put("command", "facts");
    JSONObject response = communicate(obj);
    if (response.get("values") != null && response.get("values") instanceof JSONArray) {
      for (Object val : ((JSONArray) response.get("values"))) {
        res.add(Value.fromJSON((JSONObject) val));
      }
    }
    return res;
  }
  
  public void display() {
    for (Value v : get_facts())
     System.out.println(v.toString());
  }
  
  private ServerState get_status() {
    JSONObject obj = new JSONObject();    
    obj.put("command", "status");
    JSONObject response = communicate(obj);
    if(response.get("response").equals("success")) {
      return new ServerState(response);
    }
    return null;
  }
  
  public int get_state_id() {
    ServerState status = this.get_status();
    if (status == null) {
      System.out.println("Could not get state identifier, using -1 instead");
      return -1;
    }
    return status.getSID();
  }

  public void print_violations(ServerState state) {
    state.print_violations();
  }

  public void print_violations() {
    this.get_status().print_violations();
  }
  
  private int getRandomPort() {

      int port = -1;

      Random r = new Random();

      while (port == -1) {

          int p = PORT_MIN_NUM + r.nextInt(PORT_MAX_NUM - PORT_MIN_NUM);

          if (available(p))
              port = p;

      }

      return port;
  }

  private boolean available(int port) {
      if (port < PORT_MIN_NUM || port > PORT_MAX_NUM) {
          throw new IllegalArgumentException("Invalid start port: " + port);
      }

      ServerSocket ss = null;
      DatagramSocket ds = null;
      try {
          ss = new ServerSocket(port);
          ss.setReuseAddress(true);
          ds = new DatagramSocket(port);
          ds.setReuseAddress(true);
          return true;
      } catch (IOException e) {
      } finally {
          if (ds != null) {
              ds.close();
          }

          if (ss != null) {
              try {
                  ss.close();
              } catch (IOException e) {
                  /* should not be thrown */
              }
          }
      }

      return false;
  }


}


