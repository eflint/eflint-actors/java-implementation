package util;

import org.json.simple.*;

public class Assignment {
  final public Value value;
  final public boolean assignment;

  public Assignment(Value value, boolean assignment) {
    this.value = value; this.assignment = assignment;
  }
  
  public Assignment(Value value) {
    this(value, true);
  }

  public JSONObject toJSON () {
    JSONObject obj = new JSONObject();
    obj.put("value", value.toJSON());
    obj.put("assignment", assignment);
    return obj;
  }
  
  public static Assignment fromJSON (JSONObject obj) {
    if (obj.containsKey("value") && obj.containsKey("assignment") && obj.get("assignment") instanceof Boolean && obj.get("value") instanceof JSONObject) {
      Value val = Value.fromJSON((JSONObject) obj.get("value"));
      boolean ass = (Boolean) obj.get("assignment");
      return new Assignment(val, ass);
    }
    return null;
  }
}
