package util;

import java.util.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import java.util.stream.Collectors;

public class Value {
  final public String fact_type, string_value;
  final public Integer int_value;
  final public ArrayList<Value> arguments;

  public Value(String type, String val) {
    this.fact_type = type;
    this.string_value = val;
    this.int_value = null;
    this.arguments = null;
  }

  public Value(String type, int val) {
    this.fact_type = type;
    this.int_value = val;
    this.string_value = null;
    this.arguments = null;
  }

  public Value(String type, Value... args) {
    this.fact_type = type;
    this.string_value = null;
    this.int_value = null;
    this.arguments = new ArrayList<Value>(Arrays.asList(args));
  }

  public String toString() {
    if (int_value != null) return fact_type + "(" + int_value + ")";
    if (string_value != null) return fact_type + "(" + string_value + ")";
    return fact_type + "(" + arguments.stream().map(a -> a.toString()).collect(Collectors.joining(",")) + ")";
  }

  public JSONObject toJSON () {
    JSONObject obj = new JSONObject();
    obj.put("fact-type", fact_type);
    if (int_value != null) {
      obj.put("value", int_value);
    }else if (string_value != null) {
      obj.put("value", string_value);
    }else {
      JSONArray json_rels = new JSONArray();
      for (Value rel : arguments)
        json_rels.add(rel.toJSON());
      obj.put("value", json_rels);
    }
    return obj;
  }

  public static Value fromJSON(JSONObject obj) {
    if (obj.containsKey("fact-type") && obj.containsKey("arguments")) {
      JSONArray arguments = (JSONArray) obj.get("arguments");
      Value[] args = new Value[arguments.size()];
      int i = 0;
      for (Object arg : arguments) {
        args[i++] = Value.fromJSON((JSONObject) arg);
      }
      return new Value((String) obj.get("fact-type"), args);
    }
    if (obj.containsKey("fact-type") && obj.containsKey("value") && obj.get("value") instanceof Long) {
        return new Value((String) obj.get("fact-type"), ((Long) obj.get("value")).intValue());
    }
    if (obj.containsKey("fact-type") && obj.containsKey("value")) {
        if (obj.get("value") instanceof String)
          return new Value((String) obj.get("fact-type"), (String) obj.get("value"));
        else
          return new Value((String) obj.get("fact-type"), Value.fromJSON((JSONObject) obj.get("value")));
    }
    return null;
  }

  public boolean equals(Object other) {
    if (other instanceof Value) {
      Value o = (Value) other;
      return  this.fact_type.equals(o.fact_type) && 
              (this.string_value == null && o.string_value == null || 
                this.string_value != null && o.string_value != null && this.string_value.equals(o.string_value)) &&
              (this.int_value == null && o.int_value == null || 
                this.int_value != null && o.int_value != null && this.int_value.equals(o.int_value)) &&
              (this.arguments == null && o.arguments == null || 
                this.arguments != null && o.arguments != null && this.arguments.equals(o.arguments));
    }
    else return false;
  }
}


