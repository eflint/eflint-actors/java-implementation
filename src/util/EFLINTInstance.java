package util;

public interface EFLINTInstance {

  public EFLINT eFLINT();
  
  default public void close_eFLINT() {
    this.eFLINT().close();
  }
}
