package util;

import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TransInfo {
  
  public final Value value;
  public final List<Assignment> assignments;
  public final boolean enabled;
  public final boolean is_action;
  public final List<TransInfo> synced_with;

  public TransInfo(Value value, List<Assignment> assignments, boolean enabled, boolean is_action, List<TransInfo> syncs) {
    this.value = value;
    this.assignments = assignments;
    this.enabled = enabled;
    this.is_action = is_action;
    this.synced_with = syncs;
  }
  
  public static TransInfo fromJSON(JSONObject obj) {
    if (obj.containsKey("trans-tagged") && obj.get("trans-tagged") instanceof JSONObject || 
        obj.containsKey("trans-assignments") && obj.get("trans-assignments") instanceof JSONArray ||
        obj.containsKey("trans-forced") && obj.get("trans-forced") instanceof Boolean ||
        obj.containsKey("trans-is-action") && obj.get("trans-is-action") instanceof Boolean ||
        obj.containsKey("trans-syncs") && obj.get("trans-syncs") instanceof JSONArray) {
      Value value = Value.fromJSON((JSONObject) obj.get("trans-tagged"));
      List<Assignment> assignments = (List<Assignment>) ((JSONArray) obj.get("trans-assignments")).stream().map(o -> Assignment.fromJSON((JSONObject) o)).collect(Collectors.toList());
      boolean enabled = (Boolean) obj.get("trans-forced");
      boolean is_action = (Boolean) obj.get("trans-is-action");
      List<TransInfo> synced_with = (List<TransInfo>) ((JSONArray) obj.get("trans-syncs")).stream().map(o -> TransInfo.fromJSON((JSONObject) o)).collect(Collectors.toList());
      return new TransInfo(value, assignments, enabled, is_action, synced_with);
    }
    return null;
  }

}
