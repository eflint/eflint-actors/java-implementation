package util;

public class DutyViolation extends Violation {
  public final Value duty;
  public DutyViolation(Value val) {
    duty = val;
  }
  
  @Override
  public void print() {
    System.out.println("violated duty! " + duty.toString());
  }
}

