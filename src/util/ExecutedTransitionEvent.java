package util;

public class ExecutedTransitionEvent {

  public final TransInfo info;

  public ExecutedTransitionEvent(TransInfo info) {
    this.info = info;
  }
}
