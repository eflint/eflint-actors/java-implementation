package util;

public class InvalidEFlintTransitionAttempt extends Exception {

  /*
  * null means non-deterministic transition attempt, disabled transition otherwise
  */
  public final TriggerViolation violation;
  
  public InvalidEFlintTransitionAttempt(TriggerViolation violation) {
    this.violation = violation;
  }

  public InvalidEFlintTransitionAttempt() {
    this.violation = null;
  }
}
