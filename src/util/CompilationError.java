package util;

public class CompilationError extends EFLINTError {

  public final String message;
  public CompilationError(String msg) {
    this.message = msg;
  } 
}
