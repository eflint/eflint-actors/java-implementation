package util;

import java.util.List;

public class MissingInputRequired extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 5242998367879082478L;
  public final List<Value> missing_values;
  
  public MissingInputRequired(List<Value> values) {
    this.missing_values = values;
  }
}
