package util;

public class TriggerViolation extends Violation {
  public final TransInfo info;
  
  public TriggerViolation(TransInfo info) {
    this.info = info;
  }

  @Override
  public void print() {
    System.out.println("violated action! " + info.value.toString());
  }
}
