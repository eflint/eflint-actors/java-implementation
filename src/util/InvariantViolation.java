package util;

public class InvariantViolation extends Violation {
  public final String invariant;
  public InvariantViolation(String invariant) {
    this.invariant = invariant;
  }

  public void print() {
    System.out.println("violated invariant! " + invariant);
  }
}
