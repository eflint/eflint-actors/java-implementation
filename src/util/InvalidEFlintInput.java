package util;

public class InvalidEFlintInput extends RuntimeException {
  public InvalidEFlintInput() {
    super();
  }
  public InvalidEFlintInput(String message) {
    super(message); 
  }
  public InvalidEFlintInput(String message, Throwable cause) {
    super(message, cause); 
  }
}