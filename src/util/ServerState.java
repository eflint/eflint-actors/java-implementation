package util;

import java.util.*;
import org.json.simple.*;

public class ServerState {
  private final int id;
  private final int old;
  private final ArrayList<Violation> violations = new ArrayList<Violation>();
  private final ArrayList<ExecutedTransitionEvent> out_events = new ArrayList<ExecutedTransitionEvent>();
  private final ArrayList<EFLINTError> errors = new ArrayList<EFLINTError>();
  private final ArrayList<Value> new_duties = new ArrayList<Value>();
  private final ArrayList<Value> term_duties = new ArrayList<Value>();
  private final ArrayList<Value> all_duties = new ArrayList<Value>();
  private final ArrayList<Value> new_enabled = new ArrayList<Value>();
  private final ArrayList<Value> new_disabled = new ArrayList<Value>();
  private final ArrayList<Assignment> assignments_made = new ArrayList<Assignment>();
  public Boolean query_res = null;
  private List<Value> inst_query_ress = new ArrayList<Value>();

  public ServerState(JSONObject response) {
    this.id = ((Long) response.get("new-state")).intValue();
    this.old = ((Long) response.get("old-state")).intValue();
    
    Object m_violations = response.get("violations");
    if (m_violations != null && m_violations instanceof JSONArray) {
      for (Object violation : (JSONArray) m_violations) {
        if (violation instanceof JSONObject) {
          violations.add(build_violation_obj((JSONObject) violation));
        }
      }
    }
    Object m_out_events = response.get("output-events");
    if (m_out_events != null && m_out_events instanceof JSONArray) {
      for (Object out_event : (JSONArray) m_out_events) {
        if (out_event instanceof JSONObject) {
          this.out_events.add(build_out_event_obj((JSONObject) out_event));
        }
      }
    }
    Object m_errors = response.get("errors");
    if (m_errors != null && m_errors instanceof JSONArray) {
      for (Object error : (JSONArray) m_errors) {
        if (error instanceof JSONObject) {
          this.errors.add(build_error_obj((JSONObject) error));
        }
      }
    }
    Object m_new_duties = response.get("new-duties");
    if (m_new_duties != null && m_new_duties instanceof JSONArray) {
      for (Object new_duty : (JSONArray) m_new_duties) {
        this.new_duties.add(Value.fromJSON((JSONObject) new_duty)); 
      }
    }
    Object m_term_duties = response.get("terminated-duties");
    if (m_term_duties != null && m_term_duties instanceof JSONArray) {
      for (Object term_duty : (JSONArray) m_term_duties) {
        this.term_duties.add(Value.fromJSON((JSONObject) term_duty)); 
      }
    }
    Object m_all_duties = response.get("all-duties");
    if (m_all_duties != null && m_all_duties instanceof JSONArray) {
      for (Object new_duty : (JSONArray) m_all_duties) {
        this.all_duties.add(Value.fromJSON((JSONObject) new_duty)); 
      }
    }
    Object m_new_enabled = response.get("new-enabled-transitions");
    if (m_new_enabled != null && m_new_enabled instanceof JSONArray) {
      for (Object new_trigger : (JSONArray) m_new_enabled) {
        this.new_enabled.add(Value.fromJSON((JSONObject) new_trigger)); 
      }
    }
    Object m_new_disabled = response.get("new-disabled-transitions");
    if (m_new_disabled != null && m_new_disabled instanceof JSONArray) {
      for (Object new_trigger : (JSONArray) m_new_disabled) {
        this.new_disabled.add(Value.fromJSON((JSONObject) new_trigger)); 
      }
    }

    Object m_query_ress = response.get("query-results");
    if (m_query_ress != null && m_query_ress instanceof JSONArray) {
      for (Object m_query_res : (JSONArray) m_query_ress) {
        if (m_query_res instanceof String) {
          query_res = ((String) m_query_res).equals("success");
        }
      }
    }

    Object m_inst_query_ress = response.get("inst-query-results");
    if (m_inst_query_ress != null && m_inst_query_ress instanceof JSONArray) {
      for (Object m_inst_query_res : (JSONArray) m_inst_query_ress) {
        inst_query_ress.add(Value.fromJSON((JSONObject) m_inst_query_res ));
      }
    }
    
    Object m_created = response.get("created_facts");
    if(m_created != null && m_created instanceof JSONArray) {
      for (Object created : (JSONArray) m_created) {
        assignments_made.add(new Assignment(Value.fromJSON((JSONObject) created), true));
      }
    }
    Object m_terminated = response.get("terminated_facts");
    if(m_terminated != null && m_terminated instanceof JSONArray) {
      for (Object terminated : (JSONArray) m_terminated) {
        assignments_made.add(new Assignment(Value.fromJSON((JSONObject) terminated), false));
      }
    }
  }

  private static Violation build_violation_obj(JSONObject violation) {
    if (violation.get("violation") == null) return null;
    if (violation.get("violation").equals("duty")) {
      return new DutyViolation(Value.fromJSON((JSONObject) violation.get("value")));
    }
    if (violation.get("violation").equals("invariant")) {
      return new InvariantViolation(violation.get("invariant").toString());
    }    
    if (violation.get("violation").equals("trigger")) {
      return new TriggerViolation(TransInfo.fromJSON((JSONObject) violation.get("info")));
    }
    return null;
  }

  public static ExecutedTransitionEvent build_out_event_obj(JSONObject out_event) {
    TransInfo info = TransInfo.fromJSON((JSONObject) out_event);
    if (info != null)
      return new ExecutedTransitionEvent(info);
    return null;
  }

  public static EFLINTError build_error_obj(JSONObject error) {
    if (error.get("error-type") == null) return null;
    if (error.get("error-type").equals("non-deterministic transition")) {
      return new NonDeterministicTransition();
    }
    if (error.get("error-type").equals("compilation error")) {
      return new CompilationError((String) error.get("error"));
    }
    return null;
  }

  public int getSID() {
    return id;
  }
  
  public int getOldSID() {
    return this.old;
  }

  public boolean lastQuerySucceeded() {
    return query_res != null && query_res;
  }

  public ArrayList<Violation> get_violations() {
    return this.violations;
  }
 

  public ArrayList<ExecutedTransitionEvent> get_output_events() {
    return this.out_events;
  }

  public ArrayList<Value> get_new_duties() {
    return this.new_duties;
  }
  
  public ArrayList<Value> get_terminated_duties() {
	  return this.term_duties;
  }
  
  public ArrayList<Value> get_all_duties() {
    return this.all_duties;
  }

  public ArrayList<Value> get_new_enabled() {
    return this.new_enabled;
  }

  public ArrayList<Value> get_new_disabled() {
    return this.new_disabled;
  }

  public ArrayList<EFLINTError> get_errors() {
    return this.errors;
  }
 
  public ArrayList<Assignment> get_made_assignments() {
    return this.assignments_made;
  }

  public ServerState print_violations() {
    for (Violation violation : this.violations) {
      violation.print();
    }
    return this;
  }

  public ServerState print_new_duties() {
    for (Value new_duty : this.new_duties) {
      System.out.println("new duty:" + new_duty);
    }
    return this;
  }

  public List<Value> get_query_instances() {
    return inst_query_ress;
  }
}


