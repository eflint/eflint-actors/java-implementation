package util;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;

public class Tester {
  public static void main(String[] args) {

    JSONObject obj = new JSONObject();
    ArrayList<Assignment> input = new ArrayList<Assignment>();
    input.add(new Assignment(new Value("x", new Value("int", 5)), true));
    obj.put("input", input.parallelStream().map(a -> a.toJSON()).collect(Collectors.toList()));
    System.out.println(obj.toJSONString());
  }
}
