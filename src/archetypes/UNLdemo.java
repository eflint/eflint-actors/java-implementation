package archetypes;

import util.EFLINT;
import util.Value;

public class UNLdemo {
  public static void main(String[] args) {
   
    request(false,"UvA", 42);

    request(true,"UU", 43);
  }

  private static void request(boolean brokered, String string, int request_id) {
    EFLINT instance = new EFLINT("/home/thomas/Desktop/dex_archetypes/thomas-version/hub-based-fml/unl_marten_demo.eflint");
    if (brokered) instance.phrase("+brokered-requests");
    instance.phrase("request(" + string + ")");
    for (Value edge : instance.instance_query("archetype-edge(sender,receiver,Output,application)")) {
      String view_name = edge.arguments.get(3).string_value.equals("Accumulation") ? "VIEW_UNION_" + request_id : (
                           edge.arguments.get(3).string_value.equals("Query") ? "VIEW_RESULT_" + request_id 
                             : "VIEW_LOCAL_" + edge.arguments.get(3).string_value + "_" + request_id);
      System.out.println("GRANT SELECT,INSERT ON " + view_name + " TO " + edge.arguments.get(0).string_value);
      System.out.println("GRANT SELECT ON "        + view_name + " TO " + edge.arguments.get(1).string_value);
    }
    instance.close();
  }
}
