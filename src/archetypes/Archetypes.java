package archetypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.EFLINT;
import util.Value;

public class Archetypes {

  static final String[] colors = { "red", "purple", "gray", "pink", "green", "black" };

  static public List<Edge> get_edges(EFLINT instance) {
    List<Edge> edges = new ArrayList<Edge>();
    Map<String,String> color_mapping = new HashMap<String,String>();
    for (Value v : instance.instance_query("archetype-edge")) {
        String color;
        String label;
        String event = v.arguments.get(3).string_value;
        String resource = v.arguments.get(2).string_value;

        label = event;
        color = resource.equals("Input") ? "orange" : 
                resource.equals("Algorithm") ? "red" : 
                resource.equals("Output") ? "purple" : "gray";
        
        /*
        String event = v.arguments.get(3).string_value;
        if (color_mapping.containsKey(event))
          color = color_mapping.get(event);
        else {
          color = colors[color_mapping.size()];
          color_mapping.put(event, color);
        }
        */
        edges.add(new Edge(v.arguments.get(0).string_value, v.arguments.get(1).string_value, color, label));
    }
    return edges;
  }
  
  public static void main(String[] args) {
    //EFLINT instance = new EFLINT("/home/thomas/Desktop/dex_archetypes/thomas-version/compute-to-n/scenario1.eflint");
    //EFLINT instance = new EFLINT("/home/thomas/Desktop/dex_archetypes/thomas-version/hub-based-fml/scenario1.eflint");
    EFLINT instance = new EFLINT("/home/thomas/Desktop/dex_archetypes/thomas-version/hub-based-fml/unl_marten_demo.eflint");
    //EFLINT instance = new EFLINT("/home/thomas/Desktop/dex_archetypes/thomas-version/private_operation/private_operation.eflint");
    System.out.println("digraph Archetype {");
    for (Edge edge : get_edges(instance))
//      if (!edge.source.equals(edge.target)) //comment out for self-edges
        System.out.println(edge.toString());
    System.out.println("}");
  }
}
