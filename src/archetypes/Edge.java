package archetypes;

class Edge {
  public final String source;
  public final String target;
  public final String type;
  public final String label;
  
  public Edge(String source, String target, String type, String label) {
    this.source = source; this.target = target; this.type = type; this.label = label;
  }
  
  public String toString() {
    return source + " -> " + target + " [color=" + type + "," + "label=" + label +"]"; 
  }
  
  
}