package inputLoop;

import java.util.ArrayList;
import java.util.List;

import util.Assignment;
import util.EFLINT;
import util.MissingInputRequired;
import util.Value;

public class InputLoop {

  public static void main(String[] args) {
    EFLINT eflint = new EFLINT("src/inputLoop/multiple_required.eflint");
    
    List<Assignment> input = new ArrayList<Assignment>();
    
    for (int i = 0; true; i++) {
      try {
        System.out.println(eflint.test_present(new Value("query", "Query"), input));
        System.out.println(i);
        break;
      }
      catch(MissingInputRequired e) {
        for (Value v : e.missing_values) {
          input.add(new Assignment(v, true));
        }
      }
    }
  }
}
