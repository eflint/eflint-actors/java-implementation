package externalKB;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import util.EFLINT;
import util.EFLINTInstance;
import util.ServerState;
import util.Value;
import util.Assignment;

public class Even implements EFLINTInstance {

  public static void main(String[] args) {
    Even even = new Even();
    
    even.expand();    
    even.expand();    
    even.expand();
    even.expand();
    even.expand();
    
    System.out.println(even.kb.entrySet().stream().map(e -> e.getKey().int_value).max(Integer::compare).get());
  }

  private EFLINT eflint;
  private final Map<Value,Boolean> kb = new HashMap<Value,Boolean>();

  public Even() {
    this.eflint = new EFLINT("src/externalKB/even.eflint");
    kb.put(new Value("even",0),true);
  }
  
  @Override
  public EFLINT eFLINT() {
    return this.eflint;
  }
  
  private void expand() {
    ServerState ss = this.eflint.try_phrase("expand()", toInput(this.kb));
    for (Assignment ass : ss.get_made_assignments()) {
      kb.put(ass.value, ass.assignment);
    }
  }
  
  private static List<Assignment> toInput (Map<Value,Boolean> kb) {
    return kb.entrySet().stream().filter(e -> e.getValue()).map(e -> new Assignment(e.getKey(),true)).collect(Collectors.toList());
  }
}
