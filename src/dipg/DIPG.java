package dipg;

import java.util.ArrayList;
import java.util.List;

import util.Assignment;
import util.EFLINT;
import util.ServerState;
import util.Value;

public class DIPG {

  public static void main (String[] args) {
    EFLINT instance = new EFLINT("src/dipg/eflint/access_union.eflint");
    List<Assignment> input = new ArrayList<Assignment>();
    int initial_id = instance.get_state_id();

    scenario1a(instance, input);
    instance.revert(initial_id); //undo previous scenario
    scenario1b(instance, input);
    scenario2(instance, input);
    
    instance.close();
  }

  private static void scenario1a(EFLINT instance, List<Assignment> input) {
   
    // the following information is available at the hospital
    // 1) information about HospitalA
    instance.phrase("+member(HospitalA)", input); // A is a member of the DIPG network
    instance.phrase("+affiliated-with(John, HospitalA)", input);
    
    // 2) information about the patients/donors
    instance.phrase("+donor(Alice)", input);
    instance.phrase("give-consent(Alice,HospitalA,DIPGResearch)", input); // Alice and Bob are patients and have both given consent for DIPGResearch to A 
    instance.phrase("+donor(Bob)", input);
    instance.phrase("give-consent(Bob,HospitalA,DIPGResearch)", input); // remove consent to see query fail
    
    // 3) information about the dataset X1. provided by hospital, or computed from the dataset?
    instance.phrase("+accurate-for-purpose(X1, DIPGResearch)", input);
    instance.phrase("dataset(X1)");
    instance.phrase("+coded(X1)", input);
    instance.phrase("+subject-of(Alice,X1)", input);
    instance.phrase("+subject-of(Bob,X1)", input); // or remove Bob as subject as well, to see query succeed even without consent
    
    System.out.println(instance.query_phrase("?Enabled(write(John,X1))", input)); 
  }
  
  //variation of above using input mechanism, does not require reverting as no events/actions are triggered
  private static void scenario1b(EFLINT instance, List<Assignment> input) {
    // the following information is available at the hospital
    // 1) information about HospitalA
    input.add(new Assignment(new Value("member", "HospitalA"), true)); // A is a member of the DIPG network
    input.add(new Assignment(new Value("affiliated-with", new Value("person", "John"), new Value("member", "HospitalA")), true));
    
    // 2) information about the patients/donors
    input.add(new Assignment(new Value("donor", "Alice"), true));
    input.add(new Assignment(new Value("consent", new Value("subject", "Alice")
                                                , new Value("controller", "HospitalA")
                                                , new Value("purpose", "DIPGResearch")),true));
    input.add(new Assignment(new Value("donor", "Bob"), true));
   // remove consent to see query fail
    input.add(new Assignment(new Value("consent", new Value("subject", "Bob")
                                                , new Value("controller", "HospitalA")
                                                , new Value("purpose", "DIPGResearch")),true));
    
    // 3) information about the dataset X1. provided by hospital, or computed from the dataset?
    input.add(new Assignment(new Value("accurate-for-purpose", new Value("data", "X1"), new Value("purpose", "DIPGResearch"))));
    input.add(new Assignment(new Value("coded", new Value("actual_dataset", "X1"))));
    input.add(new Assignment(new Value("subject-of", new Value("subject", "Alice"), new Value("data", "X1"))));
    // or remove Bob as subject as well, to see query succeed even without consent
    input.add(new Assignment(new Value("subject-of", new Value("subject", "Bob"), new Value("data", "X1")))); 

    System.out.println(instance.query_phrase("?Enabled(write(John,X1))", input));
    input.clear();
  }
  


  private static void scenario2(EFLINT instance, List<Assignment> input) {
	ServerState ss; // for testing purposes
    scenario1a(instance, input);
    
    instance.phrase("make-data-available(HospitalA, DCOG, X1)");
        
    // 4. project P1 by Y is proposed and then approved
    instance.phrase("+member(HospitalB)");
    instance.phrase("+affiliated-with(Eve, HospitalB)");
    instance.phrase("propose-project(HospitalB,EC,P1)"); //EC is executive committee, P1 is 'project 1'
    instance.phrase("approve-project(EC,HospitalB,P1)");
    instance.phrase("send-letter-of-approval(DIPG,HospitalB,P1)");
    ss = instance.phrase("sign-letter-of-approval(HospitalB,EC,P1)");
    System.out.println("duties: " + ss.get_all_duties());
    System.out.println("terminated duties: " + ss.get_terminated_duties()); 
    // X1 is selected for the project, 
    ss = instance.phrase("select-data(EC,HospitalB,P1,X1)"); // remove to see query below fail
    System.out.println("duties: " + ss.get_all_duties());
    System.out.println("terminated duties: " + ss.get_terminated_duties()); 

    // 5. selection of X1 and X2 creates collection `asset(P1)` and access for Y is inferred from projects' approval 
    System.out.println(instance.query_phrase("?Enabled(read(Eve,X1))"));
    
    // 6. data being sent
    ss = instance.phrase("send-data(EC, HospitalB, X1).");
    System.out.println("duties: " + ss.get_all_duties());
    System.out.println("terminated duties: " + ss.get_terminated_duties()); 
    
  }
}
