package banking;

import java.util.HashSet;
import java.util.Set;

import banking.Bank.Risk;
import util.Value;

public class DiligentEmployee extends Employee {

  public DiligentEmployee() {
    super("DiligentEmployee");
  }

  @Override
  public void perform_risk_analysis(Bank bank, ClientProfile profile) {
    Set<Risk> options = new HashSet<Risk>();
    for (Risk risk : Risk.values()) {
      if(bank.eFLINT().enabled(new Value("assign-risk", new Value("employee", this.getName())
          , new Value("client", profile.getName()), new Value("risk", risk.ordinal())))) {
        options.add(risk);
      }
    }
    Risk result = choose_risk(bank, profile, options);
    bank.receive_risk_result(this, profile, result);
  }
  
  public Risk choose_risk(Bank bank, ClientProfile profile, Set<Risk> options){
    int risk_score = Risk.LOW.ordinal();
    for (String property : profile.getProperties()) {
      Risk risk = bank.get_risk_assignment(property, profile.getPropertyValue(property));
      if (risk != null) {
        risk_score = Math.max(risk.ordinal(), risk_score);
      }
    }
    return Risk.values()[risk_score];
  }
}
