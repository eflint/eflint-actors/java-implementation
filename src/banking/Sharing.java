package banking;

class Sharing {

  public static void main(String[] args) {
    Bank a = new BankA(new DiligentEmployee());
    Bank b = new BankB(new DiligentEmployee());
    PropertySharing sharing = new PropertySharing(a, b);

    a.receive_client_application(Config.scanner.next());
    sharing.eFLINT().print_violations();
    b.receive_client_application(Config.scanner.next());
    sharing.eFLINT().print_violations();

    //a.receive_property_change(Config.scanner.next(), Config.scanner.next(), Config.scanner.next());
    b.receive_property_change(Config.scanner.next(), Config.scanner.next(), Config.scanner.next());
    sharing.eFLINT().print_violations();
    
    a.close_eFLINT();
    b.close_eFLINT();
    sharing.close_eFLINT();
    Config.scanner.close();
  }
}