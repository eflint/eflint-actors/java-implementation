package banking;

import banking.Bank.Risk;

public class BankA extends Bank {
  public BankA(Employee employee) {
    super("src/banking/risk_a.csv", "src/banking/sensitivity_a.csv", employee);
  }

  protected void consider_sharing(ClientProfile profile, Risk risk) {
    for (String property : profile.getProperties()) {
      if (sharing != null)
        sharing.receive_property_value(this, profile.getName(), property, profile.getPropertyValue(property));
    }
  }
  
  @Override
  public String getName() {
    return "BankA";
  }
}
