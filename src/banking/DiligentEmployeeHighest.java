package banking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import banking.Bank.Risk;

public class DiligentEmployeeHighest extends DiligentEmployee {

  @Override
  public Risk choose_risk(Bank bank, ClientProfile profile, Set<Risk> options){
    List<Risk> risk_list = new ArrayList<Risk>(options);
    Collections.sort(risk_list, Collections.reverseOrder());
    return risk_list.get(0);
  }

}
