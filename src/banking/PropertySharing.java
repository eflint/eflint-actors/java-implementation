package banking;

import java.util.ArrayList;

import banking.Bank.Risk;
import util.EFLINT;
import util.EFLINTInstance;
import util.InvalidEFlintInput;
import util.InvalidEFlintTransitionAttempt;
import util.Value;

public class PropertySharing implements EFLINTInstance{

  private final ArrayList<Record> sharing_record = new ArrayList<Record>();
  
  private final EFLINT eflint;

  private final Bank partyA;
  private final Bank partyB;
  
  public PropertySharing(Bank a, Bank b) {
    this.eflint = new EFLINT("src/banking/bank_sharing.eflint");
    this.partyA = a;
    this.partyB = b;
    a.setSharingAgreement(this);
    b.setSharingAgreement(this);    
    for (Risk r : Risk.values()) {
      eflint.create(new Value("risk", r.ordinal()));
      eflint.create(new Value("sensitivity", r.ordinal()));
    }
    eflint.create(new Value("bank", a.getName()));
    eflint.create(new Value("bank", b.getName()));
  }

  public void receive_property_value(Bank bank, String client, String property, String value) {
    this.sharing_record.add(new Record(bank, client, property, value));
    Bank other = bank == partyA ? partyB : partyA;
    try {
      eflint.create(new Value("property", property));
      eflint.create(new Value("client", client));
      eflint.action("share-property", bank.getName(), other.getName()
                   ,new Value("client", client), new Value("property", property));
      System.out.println(bank.getName() + " shared property " + property + " of client " + client + " with " + other.getName());
    } catch (InvalidEFlintTransitionAttempt e) {
      System.out.println("sharing failed: " + bank.getName() + other.getName() + client + property);
    }
  }
  
  @Override
  public EFLINT eFLINT() {
    return eflint;
  }

  public void record_sensitivity(Bank bank, String type, Risk risk) {
    eflint.create(new Value("property", type));
    eflint.create(new Value("sensitivity-assignment", new Value("bank", bank.getName())
        , new Value("property", type), new Value("sensitivity", risk.ordinal())));
  }
  
  public void record_risk(Bank bank, String client, Risk risk) {
    eflint.create(new Value("client", client));
    try {
      eflint.event(new Value("risk-assignment-changed", new Value("bank", bank.getName())
          , new Value("client", client), new Value("risk", risk.ordinal())));
    } catch (InvalidEFlintTransitionAttempt e) {
      System.out.println("DEBUG: cannot change risk");
    }
  }

}
