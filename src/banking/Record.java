package banking;

public class Record {

  private final long timestamp;
  private final String value;
  private final String client;
  private final Bank bank;
  private final String property;

  public Record(Bank bank, String client, String property, String value) {
    this.bank = bank;
    this.client = client;
    this.property = property;
    this.value = value;
    this.timestamp = System.currentTimeMillis();
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getValue() {
    return value;
  }

  public Bank getBank() {
    return bank;
  }

  public String getProperty() {
    return property;
  }

  public String getClient() {
    return client;
  }
}
