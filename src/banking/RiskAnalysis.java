package banking;

class RiskAnalysis {

  public static void main(String[] args) {
    Bank a;
    
    //a = new BankA(new DiligentEmployeeLowest());
    a = new BankA(new LazyEmployee());
    a.receive_client_application(Config.scanner.next());
    a.receive_client_application(Config.scanner.next());
    a.close_eFLINT();
    
    Config.scanner.close();
  }
}

