package banking;

public class BankB extends Bank {

  public BankB(Employee employee) {
    super("src/banking/risk_b.csv", "src/banking/sensitivity_b.csv", employee);
  }

  @Override
  protected void consider_sharing(ClientProfile profile, Risk risk) {
    return;
  }
  
  @Override
  public String getName() {
    return "BankB";
  }
}
