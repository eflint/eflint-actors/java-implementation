package banking;

import java.util.Scanner;

abstract class Employee {
  private final String name;
  
  public Employee(String name) {
    this.name = name;
  }
  
  public void investigate_client(Bank bank, ClientProfile profile) {
    if (Config.interactive) System.out.print("country of " + profile.getName() + "?\n>");
    profile.setPropertyValue("country", Config.scanner.next());
    if (Config.interactive) System.out.print("sib of " + profile.getName() + "?\n>");
    profile.setPropertyValue("sib", Config.scanner.next());
    if (Config.interactive) System.out.print("email of " + profile.getName() + "?\n>");
    profile.setPropertyValue("email", Config.scanner.next());
    bank.investigation_completed(this, profile, true);
  }

  public String getName() {
    return this.name;
  }
  
  abstract public void perform_risk_analysis(Bank bank, ClientProfile profile);

}
