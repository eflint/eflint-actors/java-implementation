package banking;

import banking.Bank.Risk;

public class LazyEmployee extends Employee {

  public LazyEmployee() {
    super("LazyEmployee");
  }

  @Override
  public void perform_risk_analysis(Bank bank, ClientProfile profile) {
    bank.receive_risk_result(this, profile, Risk.LOW);
  }
}
