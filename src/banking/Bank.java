package banking;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import util.EFLINT;
import util.EFLINTInstance;
import util.InvalidEFlintInput;
import util.InvalidEFlintTransitionAttempt;
import util.Value;

public abstract class Bank implements EFLINTInstance {
  private final EFLINT eflint;

  private final HashMap<String,ClientProfile> clients = new HashMap<String,ClientProfile>();
  private final String kyc_risk_file;
  private final String sensitivity_file;
  private final HashMap<String,Risk> kyc_risk = new HashMap<String,Risk>();
  private final HashMap<String,Risk> sensitivity = new HashMap<String,Risk>();

  enum Risk { LOW, MEDIUM, HIGH }
  HashMap<String, Risk> sib_risk = new HashMap<String, Risk>();
  private HashMap<String, Risk> country_risk = new HashMap<String, Risk>();
  private Employee employee;

  protected PropertySharing sharing;
  
  public Bank(String risk_file, String sensitivity_file, Employee employee) {
    this.kyc_risk_file = risk_file;
    this.sensitivity_file = sensitivity_file;
    this.employee = employee;
    this.eflint = new EFLINT("src/banking/bank_kyc.eflint");
    // read risk file
    try(BufferedReader file_reader = new BufferedReader(new FileReader(kyc_risk_file))) {
      String risk_line;
      while ((risk_line = file_reader.readLine()) != null) {
        String[] risk_values = risk_line.split(",");
        if (risk_values.length > 3) continue;
        record_risk(risk_values[0], risk_values[1], risk_values[2]);
      }
      initialize_eflint_instance();
    } catch (FileNotFoundException e) {
      System.out.println("cannot find risk assessment file: " + kyc_risk_file);
    } catch (IOException e1) {
      System.out.println("failed to closed file reader");
      e1.printStackTrace();
    }    
  }


  public void receive_client_application(String client) {
    ClientProfile profile = new ClientProfile(client);
    this.clients.put(client, profile);
    // <qualification>
    eflint.create(new Value("client", client));
    try {
      eflint.event(new Value("require-analysis", new Value("client", client)));
    } catch (InvalidEFlintInput e) {
    } catch (InvalidEFlintTransitionAttempt e) {
    }
    // </qualification>
    this.employee.investigate_client(this, profile);
  }
  
  public void investigation_completed(Employee employee, ClientProfile profile, boolean accept) {
    if (accept) {      
      // <qualification>
      if (profile.hasProperty("sib")) {
        eflint.create(new Value("sib-of", new Value("client", profile.getName())
                                        , new Value("sib", profile.getPropertyValue("sib"))));
      }
      if (profile.hasProperty("country")) {
        eflint.create(new Value("country-of", new Value("client", profile.getName())
                                            , new Value("country", profile.getPropertyValue("country"))));
      }
      // </qualification>
      for (String property : profile.getProperties()) {
        if (critical_field(property)) {
          this.employee.perform_risk_analysis(this, profile);
          return;
        }
      }
      this.kyc_risk.put(profile.getName(), Risk.LOW);
    }
    else {
        this.clients.remove(profile.getName());
    }
  }
  
  public void receive_property_change(String client, String property, String new_value) {
    if (clients.containsKey(client)) {
      ClientProfile profile = clients.get(client);
      String old_value = profile.getPropertyValue(property);
      profile.setPropertyValue(property, new_value);
      
      if (property.equals("sib")) {
        eflint.create(new Value("sib-of", new Value("client", client)
                                        , new Value("sib", new_value)));
      }
      if (property.equals("country")) {
        eflint.create(new Value("country-of", new Value("client", client)
                                            , new Value("country", new_value)));
      }
      if (!old_value.equals(new_value) && critical_field(property)) {
        employee.perform_risk_analysis(this, profile);
      }
    }
  }
  
  
  public void receive_risk_result(Employee employee, ClientProfile profile, Risk risk) {
    Risk old_value = kyc_risk.get(profile.getName());
    if (old_value == null || !old_value.equals(risk)) {
      System.out.println(this.getName() + " assigned a risk of " + risk + " to client " + profile.getName());
      try { // see if this risk assignment is compliant with the bank's policy
        eflint.action(new Value("assign-risk", new Value("employee", employee.getName())
                     ,new Value("client", profile.getName()), new Value("risk", risk.ordinal())));
        kyc_risk.put(profile.getName(), risk);
        if (sharing != null) sharing.record_risk(this, profile.getName(), risk);
        consider_sharing(profile, risk);    
      } catch (InvalidEFlintTransitionAttempt e) {
        System.out.println(employee.getName() + " gave " + profile.getName() + " a risk value of " + risk + " which is too low");
      }
    }
  }

  protected abstract void consider_sharing(ClientProfile profile, Risk risk);

  private void initialize_eflint_instance() {
    eflint.create(new Value("employee", employee.getName()));
    for (Risk r : Risk.values()) {
      eflint.create(new Value("risk", r.ordinal()));
    }
    for (String sib_val : sib_risk.keySet()) {
      eflint.create(new Value("sib", sib_val));
    }    
    for (Entry<String,Risk> entry : sib_risk.entrySet()) {
      eflint.create(new Value("sib-risk", new Value("sib", entry.getKey()), new Value("risk", entry.getValue().ordinal())));
    }
    for (String country_val : country_risk.keySet()) {
      eflint.create(new Value("country", country_val));
    }
    for (Entry<String,Risk> entry : country_risk.entrySet()) {
      eflint.create(new Value("country-risk", new Value("country", entry.getKey()), new Value("risk", entry.getValue().ordinal())));
    }
  }
  
  @Override
  public EFLINT eFLINT() {
    return this.eflint;
  }

  public Risk get_risk_assignment(String property, String value) {
    if (property.equals("sib")) {
      return sib_risk.get(value);
    }
    if (property.equals("country")) {
      return country_risk.get(value);
    }
    return null;
  }
  
  private void record_risk(String type, String key, String risk) {
    if (type.equals("sib")) {
      this.sib_risk.put(key, Risk.valueOf(risk));
    }
    else {
      this.country_risk.put(key,  Risk.valueOf(risk));
    }
  }
  
  private void record_sensitivity(String type, String risk_str) {
    Risk risk = Risk.valueOf(risk_str);
    sensitivity.put(type, risk);
    if (sharing != null) {
      sharing.record_sensitivity(this, type, risk);
    }
  }

  private boolean critical_field(String property) {
    return property.equals("sib") || property.equals("country");
  }
  
  public void setSharingAgreement(PropertySharing sharing) {
    this.sharing = sharing;    
    
    // read sensitivity file
    try(BufferedReader file_reader = new BufferedReader(new FileReader(sensitivity_file))) {
      String risk_line;
      while ((risk_line = file_reader.readLine()) != null) {
        String[] risk_values = risk_line.split(",");
        if (risk_values.length > 2) continue;
        record_sensitivity(risk_values[0], risk_values[1]);
      }
    } catch (FileNotFoundException e) {
      System.out.println("cannot find risk assessment file: " + sensitivity_file);
    } catch (IOException e1) {
      System.out.println("failed to closed file reader");
      e1.printStackTrace();
    }
  }

  public String getName() {
    return this.toString();
  }
}
