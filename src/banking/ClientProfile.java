package banking;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class ClientProfile {
  private final String client_id;
  private final HashMap<String, String> info = new HashMap<String, String>();
  
  public ClientProfile(String client_id, Entry<String, String>... properties) {
    this.client_id = client_id;
    for(Entry<String, String> entry : properties) {
      info.put(entry.getKey(), entry.getValue());
    }
  }
  
  public String getName() {
    return client_id;
  }
  
  public boolean hasProperty(String key) {
    return info.containsKey(key);
  }
  
  public String getPropertyValue(String key) {
    return info.get(key);
  }
  
  public String setPropertyValue(String key, String value) {
    if (value != null) 
      return info.put(key, value);
    return value;
  }
  
  public Set<String> getProperties() {
    return info.keySet();
  }
}
