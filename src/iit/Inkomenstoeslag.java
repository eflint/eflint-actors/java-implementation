package iit;

import java.util.ArrayList;
import java.util.List;

import util.Assignment;
import util.EFLINT;
import util.Value;

public class Inkomenstoeslag {

  public static void main(String[] args) {
    EFLINT instance = new EFLINT("src/iit/regels.eflint");
    instance.phrase("+ambtenaar(Ambtenaar)");
    
    List<Assignment> zaak1 = new ArrayList<Assignment>();
    zaak1.add(new Assignment(new Value("[Woonplaats]", "Utrecht")));
    zaak1.add(new Assignment(new Value("[AOW leeftijd behaald]", "Ja")));
    zaak1.add(new Assignment(new Value("[Ouder dan 21]", "Ja")));
    zaak1.add(new Assignment(new Value("[Alleenstaande]", "Ja")));
    zaak1.add(new Assignment(new Value("[Thuiswonende kinderen]", "Ja")));
    zaak1.add(new Assignment(new Value("[Inkomen per maand]", 1000)));
    zaak1.add(new Assignment(new Value("[Vermogen]", 1000)));
    
    System.out.println(!instance.enabled(new Value("<Recht op IIT 51>", "Ambtenaar"), zaak1));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 231>", "Ambtenaar"), zaak1));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 591>", "Ambtenaar"), zaak1));
    
    List<Assignment> zaak2 = new ArrayList<Assignment>();
    zaak2.add(new Assignment(new Value("[Woonplaats]", "Utrecht")));
    zaak2.add(new Assignment(new Value("[AOW leeftijd behaald]", "Ja"), false));
    zaak2.add(new Assignment(new Value("[Ouder dan 21]", "Ja")));
    zaak2.add(new Assignment(new Value("[Alleenstaande]", "Ja")));
    zaak2.add(new Assignment(new Value("[Thuiswonende kinderen]", "Ja")));
    zaak2.add(new Assignment(new Value("[Inkomen per maand]", 1000)));
    zaak2.add(new Assignment(new Value("[Vermogen]", 1000)));
    
    System.out.println( instance.enabled(new Value("<Recht op IIT 51>", "Ambtenaar"), zaak2));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 231>", "Ambtenaar"), zaak2));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 591>", "Ambtenaar"), zaak2));

    List<Assignment> zaak3 = new ArrayList<Assignment>();
    zaak3.add(new Assignment(new Value("[Woonplaats]", "Utrecht")));
    zaak3.add(new Assignment(new Value("[AOW leeftijd behaald]", "Ja"), false));
    zaak3.add(new Assignment(new Value("[Ouder dan 21]", "Ja")));
    zaak3.add(new Assignment(new Value("[Alleenstaande]", "Ja"), false));
    zaak3.add(new Assignment(new Value("[Thuiswonende kinderen]", "Ja"), false));
    zaak3.add(new Assignment(new Value("[Inkomen per maand]", 1000)));
    zaak3.add(new Assignment(new Value("[Vermogen]", 1000)));
    
    System.out.println(!instance.enabled(new Value("<Recht op IIT 51>", "Ambtenaar"), zaak3));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 231>", "Ambtenaar"), zaak3));
    System.out.println( instance.enabled(new Value("<Recht op IIT 591>", "Ambtenaar"), zaak3));
    
    List<Assignment> zaak4 = new ArrayList<Assignment>();
    zaak4.add(new Assignment(new Value("[Woonplaats]", "Utrecht")));
    zaak4.add(new Assignment(new Value("[AOW leeftijd behaald]", "Ja"), false));
    zaak4.add(new Assignment(new Value("[Ouder dan 21]", "Ja")));
    zaak4.add(new Assignment(new Value("[Alleenstaande]", "Ja"), false));
    zaak4.add(new Assignment(new Value("[Thuiswonende kinderen]", "Ja"), false));
    zaak4.add(new Assignment(new Value("[Inkomen per maand]", 1000)));
    zaak4.add(new Assignment(new Value("[Vermogen]", 30000)));
    
    System.out.println(!instance.enabled(new Value("<Recht op IIT 51>", "Ambtenaar"), zaak4));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 231>", "Ambtenaar"), zaak4));
    System.out.println(!instance.enabled(new Value("<Recht op IIT 591>", "Ambtenaar"), zaak4));
    
    instance.close();
  }
}
