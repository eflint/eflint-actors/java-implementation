package offers;

public class CheckoutTest {

  public static void main(String[] args) {
    Offer offer = new Offer("Chloe", "D1");
    offer.initialise(100); // triggers violation against no componsentation obligation
    // offer.initialise(null); // no violation in this case
    offer.checkout("Bob"); // attribution post condition automatically filled in from consortium agreement
    offer.close();
  }
}
