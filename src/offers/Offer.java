package offers;

import java.util.List;
import java.util.stream.Collectors;

import util.EFLINT;
import util.ServerState;
import util.Value;

public class Offer extends EFLINT {
  
  private String assigner;
  private String asset;
  
  private ServerState currentState;

  public Offer(String assigner, String asset) {
    super("src/offers/eflint/consortium1.eflint");
    this.assigner = assigner; this.asset = asset;
  }

  public void initialise(Integer price) {
    currentState = this.phrase("initialise(" + assigner + "," + asset + ")");
    if (price != null) addPaymentPreCondition(price);
    currentState = this.phrase("publish(" + assigner + ")");
  }
  
  public void checkout(String assignee) {
    System.out.println(this.getPreConditions()); // while getPreConditions() not empty, handle them... or cancel checkout
    this.accept(assignee);
  }
  
  private void accept(String assignee) {
    currentState = this.phrase("accept(" + assignee + ")");
    System.out.println(this.getPostConditions()); // afterwards, print postconditions
  }
  
  public void addPaymentPreCondition(int price) {
    currentState = this.phrase("add-payment-pre-condition(" + assigner + "," + price + ")");
    currentState.print_violations(); // change Chloe to Alice to see violation disappear
  }

  private List<Value> getPostConditions() {
    return currentState.get_all_duties().stream().filter(v -> v.fact_type.contains("post-condition")).collect(Collectors.toList());
  }

  private List<Value> getPreConditions() {
    return currentState.get_all_duties().stream().filter(v -> v.fact_type.contains("pre-condition")).collect(Collectors.toList());
  }
}
